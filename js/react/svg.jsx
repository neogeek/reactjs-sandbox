/** @jsx React.DOM */

var SVG = React.createClass({
    getInitialState: function () {
        return { objects: { rect: [] } }
    },
    addRect: function (options) {
        this.state.objects.rect.push(options);
        this.setState({ objects: this.state.objects });
    },
    render: function () {
        var renderObject = function (item) {
            return SVGRect(item);
        };
        return <svg width={this.props.width || 500} height={this.props.height || 500}>
            {this.state.objects.rect.map(renderObject)}
        </svg>;
    }
});

var SVGRect = React.createClass({
    render: function () {
        return <rect x={this.props.x || 0} y={this.props.y || 0} width={this.props.width || 100} height={this.props.width || 100} fill={this.props.fill || "#000"} />;
    }
});
