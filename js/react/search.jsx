/** @jsx React.DOM */

var Search = React.createClass({
    loadData: function () {
        var self = this;
        $.getJSON(this.props.url).done(function (data) {
            self.state.data = data.sort(function (a, b) {
                return a.name.localeCompare(b.name);
            });
        });
    },
    getInitialState: function () {
        return {data: [], results: [], total: 0};
    },
    componentWillMount: function(){
        this.loadData();
    },
    render: function() {
        var self = this;
        var updateSearch = function (e) {
            var matches = [],
                search;
            if (e.target.value) {
                search = new RegExp(e.target.value.replace(/[^A-Za-z]+/, '|'), 'i');
                self.state.data.forEach(function (item) {
                    if (item[self.props.searchby].match(search)) {
                        matches.push(item);
                    }
                });
            }
            self.setState({results: matches.slice(0, 10), total: matches.length});
        };
        return <form>
            <fieldset>
                <input type="text" name="q" id="q" size="30" placeholder={this.props.placeholder} onKeyUp={updateSearch} />
            </fieldset>
            <div className="results">
                <SearchResults results={this.state.results} total={this.state.total} />
            </div>
        </form>;
    }
});

var SearchResults = React.createClass({
    render: function () {
        var createItem = function (item) {
            return <li className="item"><a href="#">{item.name}</a></li>;
        };
        if (this.props.results.length) {
            return <div>
                <ul>{this.props.results.map(createItem)}</ul>
                <p className="status">{this.props.total - this.props.results.length} more results found</p>
            </div>
        } else {
            return <p className="status">No results.</p>;
        }
    }
});
