/** @jsx React.DOM */

var CardList = React.createClass({

    loadData: function () {

        var self = this;

        $.getJSON(this.props.data).done(function (data) {

            self.setState({ data: data });

        });

    },

    getInitialState: function () {

        return { data: [] };

    },

    componentWillMount: function () {

        this.loadData();

    },

    render: function () {

        var self = this;

        function sortNames () {

            self.setState({
                data: self.state.data.sort(function (a, b) {
                    return a.name.localeCompare(b.name);
                })
            });

        }

        function renderCard (item) {

            return <Card key={item.id} data={item}/>;

        }

        return <div><button onClick={sortNames}>Sort</button><p>{this.state.data.length}</p>{this.state.data.map(renderCard)}</div>;

    }

});

var Card = React.createClass({

    getInitialState: function () {

        return { status: 'inactive' };

    },

    render: function () {

        var self = this;

        function setStatus () {

            self.setState({ status: 'active' });

        }

        return <div className="card" data-status={this.state.status} tabIndex="0">
            <p>{this.props.data.name}</p>
            <p>{this.state.status}</p>
            <form>
                <input type="checkbox" onClick={setStatus} />
            </form>
        </div>;

    }

});
