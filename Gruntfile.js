module.exports = function (grunt) {

    'use strict';

    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        shell: {

            reactjs: {

                command: 'jsx --extension jsx js/react/ react_components/build/; cat react_components/build/*.js > react_components/compiled.js'

            }

        },

        watch: {

            reactjs: {
                files: ['js/react/**/*.jsx'],
                tasks: ['shell:reactjs']
            }

        }

    });

    grunt.registerTask('default', [ 'shell:reactjs' ]);

};
